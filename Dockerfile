# Official Jenkins image augmented with enough to support the Docker Pipeline plugin.
FROM jenkinsci/jenkins:latest

USER root

RUN apt-get update \
  && apt-get install -y sudo \
  && adduser jenkins sudo \
  && echo 'Defaults !authenticate' >> /etc/sudoers


RUN echo 'America/Phoenix' > /etc/timezone && dpkg-reconfigure --frontend=noninteractive tzdata

RUN wget -O /tmp/docker_bin.tar.gz --no-check-certificate https://get.docker.com/builds/Linux/x86_64/docker-1.12.0.tgz && tar xzf /tmp/docker_bin.tar.gz -C /usr/bin --strip-components=1 && rm /tmp/docker_bin.tar.gz && chmod 755 /usr/bin/docker

USER jenkins

COPY jobs /usr/share/jenkins/ref/jobs/
COPY phpplus /usr/share/jenkins/ref/phpplus/
COPY tester /usr/share/jenkins/ref/tester/
COPY localweb /usr/share/jenkins/ref/localweb/

# Override the original versions.
COPY jenkins.sh /usr/local/bin/jenkins.sh
COPY install-plugins.sh /usr/local/bin/install-plugins.sh
RUN install-plugins.sh \
  ace-editor \
  antisamy-markup-formatter \
  authentication-tokens \
  bouncycastle-api \
  branch-api \
  build-timeout \
  cloudbees-folder \
  credentials-binding \
  credentials \
  docker-build-publish \
  docker-commons \
  docker-workflow \
  durable-task \
  external-monitor-job \
  git-client \
  git-server \
  git \
  github-api \
  handlebars \
  icon-shim \
  jquery-detached \
  junit \
  ldap \
  mailer \
  mapdb-api \
  matrix-auth \
  matrix-project \
  momentjs \
  pam-auth \
  pipeline-build-step \
  pipeline-input-step \
  pipeline-rest-api \
  pipeline-stage-step \
  pipeline-stage-view \
  plain-credentials \
  scm-api \
  script-security \
  ssh-credentials \
  structs \
  timestamper \
  token-macro \
  windows-slaves \
  workflow-aggregator \
  workflow-api \
  workflow-basic-steps \
  workflow-cps-global-lib \
  workflow-cps \
  workflow-durable-task-step \
  workflow-job \
  workflow-multibranch \
  workflow-scm-step \
  workflow-step-api \
  workflow-support \
  ws-cleanup

RUN echo 2.0 > /usr/share/jenkins/ref/jenkins.install.UpgradeWizard.state

CMD /usr/local/bin/jenkins.sh
