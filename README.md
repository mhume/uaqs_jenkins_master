Jenkins Docker image with Docker support
========================================

A variant of the official Jenkins Docker image with support for Docker added, allowing it to use the technique demonstated in the [Docker image for Docker Pipeline demo](https://github.com/jenkinsci/docker-workflow-plugin/tree/master/demo). It does not start its own Docker daemon, but does issue Docker commands to its parent, in particular requests to start up sister containers.
